<?php

require_once 'ObjectInterface.php';

class FakeApiClient
{
    protected $client;

    /**
     * Конструктор FakeApiClient
     * @param HttpClientInterface $client
     */
	public function __construct(HttpClientInterface $client)
    {
		$this->client = $client;
	}

    /**
     * Создание объекта
     * @param ObjectInterface $obj
     * @return mixed
     */
	public function create(ObjectInterface $obj)
    {
        $response = $this->client->post('objects', [], $obj->toArray());

        if (!$response) {
            return false;
        }

        return $obj->fillFromArray($response);
    }

    /**
     * Обновление объекта
     * @param ObjectInterface $obj
     * @return mixed
     */
    public function update(ObjectInterface $obj)
    {
        $response = $this->client->update('objects/' . $obj->getId(), [], $obj->toArray());

        if (!$response) {
            return false;
        }

        return $obj->fillFromArray($response);
    }

    /**
     * Удаление объекта
     * @param ObjectInterface $obj
     * @return mixed
     */
    public function delete(ObjectInterface $obj)
    {
        $response = $this->client->delete('objects/' . $obj->getId());

        if (!$response) {
            return false;
        }

        return $response['result'];
    }

    /**
     * Получение объекта
     * @param ObjectInterface $obj
     * @return mixed
     */
    public function get(ObjectInterface $obj)
    {
        $response = $this->client->get('objects/' . $obj->getId());

        if (!$response) {
            return false;
        }

        return $obj->fillFromArray($response);
    }
}