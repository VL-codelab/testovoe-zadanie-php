<?php

require_once 'HttpClientInterface.php';

class HttpClient implements HttpClientInterface
{
    protected $defaultOptions = [];
    protected $options = [];
    protected $threshold;
    protected $apiVersion;

    const LOG_INFO = 'INFO';
    const LOG_ERROR = 'ERROR';
    const LOG_WARNING = 'WARNING';

    /**
     * Конструктор HttpClient.
     * @param $host string Хост
     * @param $port mixed Порт
     * @param $timeout mixed Таймаут запроса
     * @param $threshold mixed Таймаут для логирования медленных запросов
     */
    public function __construct($host, $port, $timeout, $threshold)
    {
        $this->options = $this->defaultOptions = [
            CURLOPT_URL => $host,
            CURLOPT_TIMEOUT => $timeout,
            CURLOPT_PORT => $port,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_HEADER => false,
        ];

        $this->threshold = $threshold;
    }

    /**
     * @inheritdoc
     */
    public function get($path, $queryParams = [], $headers = [])
    {
        $this->setOptions([
            CURLOPT_URL => $this->buildUrl($path, $queryParams),
        ]);

        return $this->send();
    }

    /**
     * @inheritdoc
     */
    public function post($path, $queryParams = [], $body = [], $headers = [])
    {
        $this->setOptions([
            CURLOPT_URL => $this->buildUrl($path, $queryParams),
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => http_build_query($body),
        ]);

        return $this->send();
    }

    /**
     * @inheritdoc
     */
    public function put($path, $queryParams = [], $body = [], $headers = [])
    {
        $this->setOptions([
            CURLOPT_URL => $this->buildUrl($path, $queryParams),
            CURLOPT_CUSTOMREQUEST => 'PUT',
            CURLOPT_POSTFIELDS => http_build_query($body),
        ]);

        return $this->send();
    }

    /**
     * @inheritdoc
     */
    public function delete($path, $queryParams = [], $headers = [])
    {
        $this->setOptions([
            CURLOPT_URL => $this->buildUrl($path, $queryParams),
            CURLOPT_CUSTOMREQUEST => 'DELETE',
        ]);

        return $this->send();
    }

    /**
     * Метод для установки CURL опций
     * @param $opt
     * @return void
     */
    public function setOptions($opt)
    {
        $this->options = $opt + $this->options;
    }

    /**
     * Метод для формирования URL запроса
     * @param string $path Путь URL
     * @param array $queryParams GET параметры
     * @return string Сформированный путь
     */
    protected function buildUrl($path, $queryParams)
    {
        return
            rtrim($this->options[CURLOPT_URL], '/') .
            '/' .
            rtrim($path, '?') .
            (!empty($queryParams) ? '?' . http_build_query($queryParams) : '');
    }

    /**
     * Метод для отправки CURL запроса
     * @return bool|array
     */
    protected function send()
    {
        $ch = curl_init();

        curl_setopt_array($ch, $this->options);
        $str = curl_exec($ch);

        $totalTime = curl_getinfo($ch, CURLINFO_TOTAL_TIME);

        curl_close($ch);

        $response = json_decode($str, true);

        if ($totalTime > $this->threshold) {
            $this->warning('ВНИМАНИЕ! Время выполнения запроса превысило допустимую границу %s (допустимо %s)', $totalTime, $this->threshold);
        }

        if (!is_array($response)) {
            $this->error('Невозможно декодировать ответ: %s', $str);
            return false;
        }

        $this->options = $this->defaultOptions;

        return $response;
    }

    /**
     * Метод для логирования инфо сообщений
     * @param $msg string Шаблон для сообщения
     * @param mixed ...$args
     */
    protected function info($msg, ... $args)
    {
        $this->log(self::LOG_INFO, $msg, ...$args);
    }

    /**
     * Метод для логирования предупреждений
     * @param $msg string Шаблон для сообщения
     * @param mixed ...$args
     */
    protected function warning($msg, ... $args)
    {
        $this->log(self::LOG_ERROR, $msg, ...$args);
        $this->info('%s %s', $this->options[CURLOPT_URL], !empty($this->options[CURLOPT_POSTFIELDS]) ? $this->options[CURLOPT_POSTFIELDS] : '');
    }

    /**
     * Метод для логирования ошибок
     * @param $msg string Шаблон для сообщения
     * @param mixed ...$args
     */
    protected function error($msg, ... $args)
    {
        $this->log(self::LOG_WARNING, $msg, ...$args);
        $this->info('%s %s', $this->options[CURLOPT_URL], !empty($this->options[CURLOPT_POSTFIELDS]) ? $this->options[CURLOPT_POSTFIELDS] : '');
    }

    /**
     * @param $level string Уровень сообщения
     * @param $msg string Шаблон для сообщения
     * @param mixed ...$args
     */
    protected function log($level, $msg, ... $args)
    {
        file_put_contents(
            'api.log',
            sprintf("%s [%s] %s \n", date('Y-m-d H:i:s'), $level, sprintf($msg, ...$args)),
            FILE_APPEND
        );
    }
}