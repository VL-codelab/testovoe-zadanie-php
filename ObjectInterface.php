<?php

interface ObjectInterface
{
    /**
     * Метод для получения идентификатора объекта
     * @return mixed
     */
    public function getId();

    /**
     * Метод для заполнения объекта из массива
     * @param array $arr
     * @return mixed
     */
    public function fillFromArray($arr);

    /**
     * Метод для валидации объекта
     * @param array $arr
     * @return mixed
     */
    public function validate($arr);

    /**
     * Метод преобразования объекта в массив
     * @return mixed
     */
    public function toArray();
}