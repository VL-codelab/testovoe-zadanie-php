<?php

interface HttpClientInterface
{
    /**
     * Метод для выполнения GET запроса
     * @param string $path Путь URL
     * @param array $queryParams GET параметры
     * @param array $headers Заголовки запроса
     * @return bool
     */
    public function get($path, $queryParams = [], $headers = []);

    /**
     * Метод для выполнения POST запроса
     * @param string $path Путь URL
     * @param array $queryParams GET параметры
     * @param array $body Параметры тела запроса
     * @param array $headers Заголовки запроса
     * @return bool
     */
    public function post($path, $queryParams = [], $body = [], $headers = []);

    /**
     * Метод для выполнения PUT запроса
     * @param string $path Путь URL
     * @param array $queryParams GET параметры
     * @param array $body Параметры тела запроса
     * @param array $headers Заголовки запроса
     * @return bool
     */
    public function put($path, $queryParams = [], $body = [], $headers = []);

    /**
     * Метод для выполнения DELETE запроса
     * @param $path string Путь URL
     * @param array $queryParams GET параметры
     * @param array $headers Заголовки запроса
     * @return bool
     */
    public function delete($path, $queryParams = [], $headers = []);
}