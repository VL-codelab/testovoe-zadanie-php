<?php

require_once 'ObjectInterface.php';

class TestObject implements ObjectInterface
{
    private $id;
    private $name;
    private $lastName;
    private $address;

    /**
     * @inheritdoc
     */
    public function fillFromArray($arr)
    {
        if ($this->validate($arr)) {
            foreach ($arr as $key => $value) {
                if (property_exists($this, $key)) {
                    if (is_array($value)) {
                        $this->$key = (object) $value;
                    } else {
                        $this->$key = $value;
                    }
                }
            }

            return true;
        }

        return false;
    }

    /**
     * @inheritdoc
     */
    public function validate($arr)
    {
        return true;
    }

    /**
     * @inheritdoc
     */
    public function toArray()
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'last_name' => $this->lastName,
            'address'   => $this->address,
        ];
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->id;
    }
}