<?php
require_once 'TestObject.php';
require_once 'HttpClient.php';
require_once 'FakeApiClient.php';

$httpClient = new HttpClient('http://localhost', 81, 25, 15);
$fakeApiClient = new FakeApiClient($httpClient);

$testObject = new TestObject();
$fakeApiClient->create($testObject);

$testObject->fillFromArray(['name' => 'Новое имя']);
$fakeApiClient->update($testObject);

$id = $testObject->getId();
unset($testObject);

$testObject = new TestObject();
$testObject->fillFromArray(['id' => $id]);

$fakeApiClient->get($testObject);

$fakeApiClient->delete($testObject);